package com.citi.quartzpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartzpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuartzpocApplication.class, args);
	}

}
